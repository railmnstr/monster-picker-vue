import { defineComponent as _, computed as k, watch as y, openBlock as s, createBlock as d, unref as p, withModifiers as V, withCtx as a, createElementBlock as v, Fragment as x, renderList as g, renderSlot as N, createElementVNode as b, toDisplayString as B } from "vue";
import { Swiper as C, SwiperSlide as I } from "swiper/vue";
const P = { class: "monster-picker__font monster-picker__font--option" }, q = /* @__PURE__ */ _({
  __name: "PickerBase",
  props: {
    modelValue: {
      type: [Number, String, Object],
      required: !0
    },
    options: {
      type: Array,
      required: !0
    },
    loop: {
      type: Boolean,
      default: !1
    },
    optionsPerView: {
      type: [Number, String],
      default: "auto"
    },
    speed: {
      type: Number,
      default: 300
    },
    wheelSpeed: {
      type: Number,
      default: 300
    }
  },
  emits: ["update:modelValue"],
  setup(r, { emit: u }) {
    const o = r, c = u;
    let e = null;
    const i = k(() => {
      const t = o.options.findIndex((l) => l === o.modelValue);
      return t === -1 ? 0 : t;
    });
    function m(t) {
      e = t, e.slideToLoop(i.value, 0);
    }
    function f(t) {
      const l = o.options[t.realIndex];
      c("update:modelValue", l);
    }
    y(
      () => o.modelValue,
      () => {
        !e || i.value === (e == null ? void 0 : e.realIndex) || (e.slideToLoop(i.value), setTimeout(() => {
          e.loopDestroy(), e.loopCreate();
        }, o.speed));
      }
    );
    function h(t) {
      const { deltaY: l } = t, n = l > 0 ? "slideNext" : "slidePrev";
      e[n](o.wheelSpeed);
    }
    return (t, l) => (s(), d(p(C), {
      class: "monster-picker",
      direction: "vertical",
      speed: o.speed,
      "slides-per-view": r.optionsPerView,
      loop: r.loop,
      "centered-slides": "",
      "slide-to-clicked-slide": "",
      "watch-slides-progress": "",
      onInit: m,
      onSlideChange: f,
      onWheel: V(h, ["prevent"])
    }, {
      default: a(() => [
        (s(!0), v(x, null, g(o.options, (n, w) => (s(), d(p(I), {
          class: "monster-picker__option",
          key: w
        }, {
          default: a(({ isActive: S }) => [
            N(t.$slots, "option", {
              option: n,
              isActive: S
            }, () => [
              b("span", P, B(n), 1)
            ])
          ]),
          _: 2
        }, 1024))), 128))
      ]),
      _: 3
    }, 8, ["speed", "slides-per-view", "loop"]));
  }
});
export {
  q as default
};
